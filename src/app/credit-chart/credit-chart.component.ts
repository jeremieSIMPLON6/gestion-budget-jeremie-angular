import { Component, OnInit, ViewChild } from '@angular/core';
import { OperationService } from '../services/operation.service';
import { MatPaginator, MatSort } from '@angular/material';
import { TabCreditDataSource } from '../tab-credit/tab-credit-datasource';

@Component({
  selector: 'app-credit-chart',
  templateUrl: './credit-chart.component.html',
  styleUrls: ['./credit-chart.component.css']
})
export class CreditChartComponent implements OnInit {


  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: TabCreditDataSource;

  constructor(private service: OperationService) { }

  public pieChartLabels: string[];
  public pieChartData: number[];
  public pieChartType: string = 'pie';
  
  debNameCategory: string[];
  credNameCategory: string[];

  debIdCategory: number[];
  credIdCategory: number[];



  credit: number = null;
  debit: number = null;
  total: number = null;

  ngOnInit() {
    this.service.findAll().subscribe(data => {
      this.dataSource = new TabCreditDataSource(this.paginator, this.sort, data);

      // construction de la liste des categories
      let categories = []
      for (let operation of data) {
        categories[operation.category.id] = {
          name: operation.category.name,
          debit: 0,
          credit: 0
        }
      }

      for (let operation of data) {
        const isCredit = !operation.type
        let category = categories[operation.category.id]
        if (isCredit) {
          category.credit += operation.price

        }
        else {
          category.debit += operation.price

        }
      }

      let CategoriesForDebits = categories.filter(c => c.debit > 0)
      let CategoriesForCredits = categories.filter(c => c.credit > 0)

      let categoryNamesForDebit = CategoriesForDebits.map((c) => c.name)
      let categoryNamesForCredit = CategoriesForCredits.map((c) => c.name)


      let debits = CategoriesForDebits.map((c) => c.debit);
      let credits = CategoriesForCredits.map((c) => c.credit)
     

      this.pieChartLabels = categoryNamesForCredit//['categoryNamesForDebit'];
      this.pieChartData = credits//[null];


      console.log(CategoriesForDebits, categoryNamesForDebit, debits);
      console.log(CategoriesForCredits, categoryNamesForCredit, credits);





    })
      ;
  }


  // events
  public chartClicked(e: any): void {
    console.log(e);
  }

  public chartHovered(e: any): void {
    console.log(e);
  }
}



 //loop data

      // for (let operation of data) {
      //   if (!operation.type) {
      //     this.credit += operation.price;

      //mettre l'indexOf dans une variable 

      // let idx = this.credNameCategory.indexOf(operation.category.name);

      // if (idx === -1) {
      //   this.credNameCategory.push(operation.category.name);
      //   this.credIdCategory.push(operation.price);

      //ici pusher dans le credIdCategory le price de l'operation actuelle

      // } else {

      //ici, additionner à la valeur contenue dans credIdCategory à l'index 'idx' le price de l'operation actuelle

      // }


      // } else {
      //   this.debit += operation.price;
      //   this.debNameCategory.push(operation.category.name)
      // }
      // }
      // this.pieChartData = [this.credit, this.debit];



