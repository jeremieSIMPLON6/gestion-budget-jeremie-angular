import { DataSource } from '@angular/cdk/collections';
import { MatPaginator, MatSort } from '@angular/material';
import { map } from 'rxjs/operators';
import { Observable, of as observableOf, merge } from 'rxjs';
import { OperationService } from '../services/operation.service';
import { isNgTemplate } from '@angular/compiler';

// TODO: Replace this with your own data model type
export interface TabDebitItem {
  description: string;
  id: number;
  date?: string;
  price: number;
  category?: string;
  type?: boolean;
  
}

// TODO: replace this with real data from your application
const EXAMPLE_DATA: TabDebitItem[] = [
  {id: 1, description: 'Voiture', price: 50, type:true},
  {id: 2, description: 'Vélo', price: 10, type:true},
  {id: 3, description: 'Avion', price: 20, type:true},
  {id: 4, description: 'Bateau', price: 15, type:true},
  {id: 5, description: 'Sous-marin', price: 5, type:true},
];

/**
 * Data source for the TabDebit view. This class should
 * encapsulate all logic for fetching and manipulating the displayed data
 * (including sorting, pagination, and filtering).
 */
export class TabDebitDataSource extends DataSource<TabDebitItem> {


  constructor(private paginator: MatPaginator, private sort: MatSort, private data) {
    super();
  }

  /**
   * Connect this data source to the table. The table will only update when
   * the returned stream emits new items.
   * @returns A stream of the items to be rendered.
   */
  connect(): Observable<TabDebitItem[]> {
    console.log(this.data.filter(item => item.type));

    
    // Combine everything that affects the rendered data into one update
    // stream for the data-table to consume.
    const dataMutations = [
      this.data.filter(item => item.type),
      this.paginator.page,
      this.sort.sortChange
    ];

    // Set the paginators length
    this.paginator.length = this.data.length;

    return merge(...dataMutations).pipe(map(() => {
      return this.getPagedData(this.getSortedData([...this.data.filter(item => item.type)]));
    }));
  }

  /**
   *  Called when the table is being destroyed. Use this function, to clean up
   * any open connections or free any held resources that were set up during connect.
   */
  disconnect() {}

  /**
   * Paginate the data (client-side). If you're using server-side pagination,
   * this would be replaced by requesting the appropriate data from the server.
   */
  private getPagedData(data: TabDebitItem[]) {
    const startIndex = this.paginator.pageIndex * this.paginator.pageSize;
    return data.splice(startIndex, this.paginator.pageSize);
  }

  /**
   * Sort the data (client-side). If you're using server-side sorting,
   * this would be replaced by requesting the appropriate data from the server.
   */
  private getSortedData(data: TabDebitItem[]) {
    if (!this.sort.active || this.sort.direction === '') {
      return data;
    }

    return data.sort((a, b) => {
      const isAsc = this.sort.direction === 'asc';
      switch (this.sort.active) {
        case 'description': return compare(a.description, b.description, isAsc);
        case 'id': return compare(+a.id, +b.id, isAsc);
        case 'date': return compare(a.date, b.date, isAsc);
        case 'category': return compare(+a.type, +b.type, isAsc);
        case 'price': return compare(+a.price, +b.price, isAsc);

        default: return 0;
      }
    });
  }
}

/** Simple sort comparator for example ID/description columns (for client-side sorting). */
function compare(a, b, isAsc) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}

