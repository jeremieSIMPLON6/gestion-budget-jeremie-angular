import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MatPaginatorModule, MatSortModule, MatTableModule } from '@angular/material';

import { TabDebitComponent } from './tab-debit.component';

describe('TabDebitComponent', () => {
  let component: TabDebitComponent;
  let fixture: ComponentFixture<TabDebitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabDebitComponent ],
      imports: [
        NoopAnimationsModule,
        MatPaginatorModule,
        MatSortModule,
        MatTableModule,
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabDebitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should compile', () => {
    expect(component).toBeTruthy();
  });
});
