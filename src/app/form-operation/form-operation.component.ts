import { Component, OnInit, Output, Input, EventEmitter, ViewChild } from '@angular/core';
import { Operation } from '../entity/operation';
import { Category } from '../entity/category';
import { CategoryService } from '../services/category.service';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms'; 
import { MatDatepicker } from '@angular/material';


import {MAT_MOMENT_DATE_FORMATS, MomentDateAdapter} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import * as moment from 'moment';







@Component({
  selector: 'app-form-operation',
  templateUrl: './form-operation.component.html',
  styleUrls: ['./form-operation.component.css'],
})
export class FormOperationComponent implements OnInit {

  category: Category = { name: null }
  @ViewChild(MatDatepicker) datepicker: MatDatepicker<Date>;
  @Input() operation: Operation = { name: "", category: null, price: null, type: null, date: null };
  @Input() categories: Category[];
  @Output() forSubmit = new EventEmitter<Operation>();



  form: FormGroup;


  constructor(private catSer: CategoryService, private formBuilder: FormBuilder, private adapter: DateAdapter<any>) { 
    this.initForm() 
    this.ngOnInit()
    // this.moment()
  }


  



  initForm() { 
    // initialisation du formulaire 
    this.form = this.formBuilder.group({ 
      name: null,
      category: null,
      price: null,
      type: null,
      // date: this.formBuilder.group({
      //   year: null, 
      //   month: null, 
      //   date: null
      // }),
      date:null,
      
    }); 
  } 



  // moment() {
  //   this.adapter.setLocale('fr');
  // }



  ngOnInit() {
    this.catSer.findAll()
      .subscribe(data => this.categories = data);
  }



  onSubmit() {
    this.forSubmit.emit(this.form.value);
    console.log('bip')
    console.log(this.form.value)
  }

  

  addCategory() {
    this.catSer.add(this.category).
      subscribe(data => this.categories.push(data));
  }

}
