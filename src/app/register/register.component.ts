import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PasswordValidator } from '../validator/password.validator';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  form: FormGroup;
  feedback = '';

  constructor(private fb: FormBuilder, private userServ: UserService) { }

  ngOnInit() {
    this.form = this.fb.group({
      email: [
        '',
        [
          Validators.email,
          Validators.required
        ]
      ],
      password: [
        '',
        [
          Validators.minLength(4),
          Validators.required
        ]],
      passwordRepeat: ['', Validators.required]
    },
      {
        //On applique un validateur sur tout le form group pour vérifier
        //si les deux champs password correspondent
        validator: PasswordValidator.passwordMatch('password', 'passwordRepeat')
      });

  }

  register() {
    this.userServ.add(this.form.value).subscribe(
      () => this.feedback = 'Successfuly registered',
      () => 'Registration Error');

  }

}
