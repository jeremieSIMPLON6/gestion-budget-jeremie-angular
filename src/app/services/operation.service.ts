import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Operation } from '../entity/operation';
import { map } from 'rxjs/operators';
import { log } from 'util';
import * as moment from 'moment';


@Injectable({
  providedIn: 'root'
})
export class OperationService {

  private url: string = "http://localhost:8080/operation/";

  constructor(private http: HttpClient) { }


  findAll(): Observable<Operation[]> {
    return this.http.get<Operation[]>(this.url)


    .pipe( 
      //L'opération map permet de modifier la valeur contenu à l'intérieur
      //de l'observable
      map(data => {
        //ici on boucle sur les events
        for(let operation of data) {
          //et on crée un objet Date JS à partir du timestamp stocké
          //sur le server
          operation.date = moment(operation.date);
          // console.log(operation.date.getTime());
          
        }
        //Puis on retourne les données modifiées qui seront contenues
        //dans l'observable généré par le pipe
        return data;
      })
     );
  }


  add(operation: Operation): Observable<Operation> {
    console.log(operation);
    // let tmp = new Date(operation.date);
    operation.date= operation.date.format('YYYY-MM-DD');
    return this.http.post<Operation>(this.url, operation);
  }

  delete(id:number): Observable<Operation> {
    console.log("suppr")
    return this.http.delete<Operation>(this.url + id);
  }
}