import { Component, OnInit } from '@angular/core';
import { OperationService } from '../services/operation.service';
import { Operation } from '../entity/operation';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-credit',
  templateUrl: './credit.component.html',
  styleUrls: ['./credit.component.css']
})
export class CreditComponent implements OnInit {

  
  operations: Observable<Operation[]>;
  newOperation: Operation = {name: null, price:null, type:false , category: null, date:null};
  selected: Operation;



  constructor(private service:OperationService) { }

  ngOnInit() {
    this.operations = this.service.findAll();
  }


  isPassed(date:Date) {
    let now = new Date();
    
    return date.getTime() < now.getTime();
  }

}
