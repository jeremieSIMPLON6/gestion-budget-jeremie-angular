import { Component, OnInit } from '@angular/core';
import { Operation } from '../entity/operation';
import { OperationService } from '../services/operation.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {



  operations: Operation[] = [] 

  constructor(private opServ: OperationService) { }

  ngOnInit() {
  }

  addOperation(operation: Operation) {
    this.opServ.add(operation).subscribe(op => this.operations.push(op));
  }

  
}

















