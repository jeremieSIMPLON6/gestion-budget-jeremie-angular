import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { TabCompteDataSource } from './tab-compte-datasource';
import { OperationService } from '../services/operation.service';
import { isNgTemplate } from '@angular/compiler';
import { DataSource } from '@angular/cdk/table';
import { Operation } from '../entity/operation';
import { RouterLinkWithHref } from '@angular/router';
import { Category } from '../entity/category';





@Component({
  selector: 'app-tab-compte',
  templateUrl: './tab-compte.component.html',
  styleUrls: ['./tab-compte.component.css']
})
export class TabCompteComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: TabCompteDataSource;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['id', 'date', 'type', 'name', 'category', 'price', 'button'];
  row: Operation;
  



  credit: number = null;
  debit: number = null;
  total: number = null;


  constructor(private service: OperationService) { }

  ngOnInit() {
    this.service.findAll().subscribe(data => {
    this.dataSource = new TabCompteDataSource(this.paginator, this.sort, data);

      //loop data
      for (let operation of data) {
        if (!operation.type) {
          this.credit += operation.price
        } else {
          this.debit += operation.price
        }
      }
    })
      ;
  }


  deleteOperation(row) {
    this.service.delete(row.id)
      .subscribe(() => {
        this.ngOnInit();

      });
  }


}







