import { DataSource } from '@angular/cdk/collections';
import { MatPaginator, MatSort } from '@angular/material';
import { map } from 'rxjs/operators';
import { Observable, of as observableOf, merge } from 'rxjs';

// TODO: Replace this with your own data model type
export interface TabCompteItem {
  date: string;
  id: number;
  name?: string;
  category?: string;
  price?:number;
  type?:boolean;
  total?:number;
}



// TODO: replace this with real data from your application
const EXAMPLE_DATA: TabCompteItem[] = [
  {id: 1, date: 'Janvier'},
  {id: 2, date: 'Février'},
  {id: 3, date: 'Mars'},
  {id: 4, date: 'Avril'},
  {id: 5, date: 'Mai'},
  {id: 6, date: 'Juin'},
  {id: 7, date: 'Juillet'},
  {id: 8, date: 'Aout'},
  {id: 9, date: 'Septembre'},
  {id: 10, date: 'Octobre'},
  {id: 11, date: 'Novembre'},
  {id: 12, date: 'Décembre'},
 
];

/**
 * Data source for the TabCompte view. This class should
 * encapsulate all logic for fetching and manipulating the displayed data
 * (including sorting, pagination, and filtering).
 */
export class TabCompteDataSource extends DataSource<TabCompteItem> {


  constructor(private paginator: MatPaginator, private sort: MatSort, private data ) {
    super();
  }


  /**
   * Connect this data source to the table. The table will only update when
   * the returned stream emits new items.
   * @returns A stream of the items to be rendered.
   */
  connect(): Observable<TabCompteItem[]> {
    // Combine everything that affects the rendered data into one update
    // stream for the data-table to consume.
    const dataMutations = [
      observableOf(this.data),
      this.paginator.page,
      this.sort.sortChange
    ];

    // Set the paginators length
    this.paginator.length = this.data.length;

    return merge(...dataMutations).pipe(map(() => {
      return this.getPagedData(this.getSortedData([...this.data]));
    }));
  }

  /**
   *  Called when the table is being destroyed. Use this function, to clean up
   * any open connections or free any held resources that were set up during connect.
   */
  disconnect() {}

  /**
   * Paginate the data (client-side). If you're using server-side pagination,
   * this would be replaced by requesting the appropriate data from the server.
   */
  private getPagedData(data: TabCompteItem[]) {
    const startIndex = this.paginator.pageIndex * this.paginator.pageSize;
    return data.splice(startIndex, this.paginator.pageSize);
  }

  /**
   * Sort the data (client-side). If you're using server-side sorting,
   * this would be replaced by requesting the appropriate data from the server.
   */
  private getSortedData(data: TabCompteItem[]) {
    if (!this.sort.active || this.sort.direction === '') {
      return data;
    }

    return data.sort((a, b) => {
      const isAsc = this.sort.direction === 'asc';
      switch (this.sort.active) {
        case 'date': return compare(a.date, b.date, isAsc);
        case 'id': return compare(+a.id, +b.id, isAsc);
        case 'type': return compare(+a.type, +b.type, isAsc);
        case 'category': return compare(+a.type, +b.type, isAsc);
        case 'price': return compare(+a.price, +b.price, isAsc);
        case 'total': return compare(+a.total, +b.total, isAsc);

        default: return 0;
      }
    });
  }
}

/** Simple sort comparator for example ID/date columns (for client-side sorting). */
function compare(a, b, isAsc) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}

